QUERY = """
    WITH items_1 AS (
        select
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            (text2 LIKE '%%, ID')
            AND
            SIMILARITY(text1, '{filter_place}') > 0.5
        LIMIT
            3
    ), items_2 AS (
        SELECT
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            (text2 LIKE '%%, ID')
            and 
            SIMILARITY(text1, '{filter_place}') > 0.3
        LIMIT
            10
    ), items_3 AS (
        SELECT
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            (text2 LIKE '%%, ID')
            and 
            SIMILARITY(text1, '{first_word}') > 0.2
        LIMIT
            10
    ), items_4 AS (
        SELECT
            text1,
            text2
        FROM
            zztemp_codinghw2
        WHERE
            SIMILARITY(text1, '{first_word}') > 0.3
        LIMIT
            6
    ), merged AS (
        SELECT *, 1 as SortOrder FROM items_1
        UNION
        SELECT *, 2 as SortOrder FROM items_2
        UNION
        SELECT *, 3 as SortOrder FROM items_3
        UNION
        SELECT *, 4 as SortOrder FROM items_4
        ORDER BY SortOrder
    )
    SELECT text1, text2, sortorder FROM merged
"""

QUERY_SINGLE = """
    WITH items_1 AS (
        select
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            (text2 LIKE '%%, ID')
            AND
            SIMILARITY(text1, '{filter_place}') > 0.5
        LIMIT
            3
    ), items_2 AS (
        SELECT
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            (text2 LIKE '%%, ID')
            and 
            SIMILARITY(text1, '{filter_place}') > 0.2
        LIMIT
            10
    ), items_3 AS (
        SELECT
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            SIMILARITY(text1, '{filter_place}') > 0.2
        LIMIT
            10
    ), merged AS (
        SELECT *, 1 as SortOrder FROM items_1
        UNION
        SELECT *, 2 as SortOrder FROM items_2
        UNION
        SELECT *, 3 as SortOrder FROM items_3
        ORDER BY SortOrder
    )
    SELECT text1, text2, sortorder FROM merged
"""

QUERY_EQUAL = """
    WITH items_1 AS (
        select
            text1,
            text2
        FROM
            zztemp_codinghw2
        where
            LEFT(text1, {word_len}) = '{word}'
        LIMIT
            15
    )
    SELECT *, 1 as SortOrder FROM items_1
"""