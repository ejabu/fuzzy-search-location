from rest_framework import serializers
from .models_transient import Location

class LocationSerializer(serializers.ModelSerializer):
    data = serializers.CharField(source='text2', read_only=True)
    class Meta:
        model = Location
        fields = ['data']